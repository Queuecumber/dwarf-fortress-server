FROM queuecumber/dwarf-fortress-minimal

# system packages for X, vnc, pulseaudio, ssh
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    x11vnc \
    xvfb \
    pulseaudio \
    dbus-x11 \
    xinit \
    openssh-server \
    unzip \
    sudo

RUN mkdir -p /var/run/sshd

RUN echo "load-module module-native-protocol-tcp auth-anonymous=1" >> /etc/pulse/default.pa

# install dfhack
ADD https://github.com/DFHack/dfhack/releases/download/0.44.12-r1/dfhack-0.44.12-r1-Linux-64-gcc-7.tar.bz2 /dfhack.tar.bz2
RUN tar xf /dfhack.tar.bz2 -C /df_linux && \
     mv /df_linux/dfhack.init-example /df_linux/dfhack.init && \
     rm /dfhack.tar.bz2

# install CLA tileset
ADD https://github.com/DFgraphics/CLA/archive/44.xx-v25.tar.gz /cla.tar.gz
RUN tar xf /cla.tar.gz -C /df_linux --strip-components=1 && \
    rm /cla.tar.gz

# custom settings
ADD init.txt /df_linux/data/init/init.txt
ADD d_init.txt /df_linux/data/init/d_init.txt
ADD colors.txt /df_linux/data/init/colors.txt

# final configuration and run
RUN chown -R dwarf:dwarf /df_linux && \
    mkdir /tmp/.X11-unix && \
    chmod 1777 /tmp/.X11-unix

ENV DISPLAY :1

ADD launch /launch

CMD /launch